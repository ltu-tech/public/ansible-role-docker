# Ansible-role-docker

A docker.ubuntu (https://github.com/angstwad/docker.ubuntu) wrapper that provides basic Debian support.

## Configuration file

One can set the "/etc/docker/daemon.json" configuration file by setting the `daemon_config` variable. It's default value is
as follows:

```yml
daemon_config:
  log-driver: "json-file"
  log-opts:
    max-size: "25m"
    max-file: "2"
```

Note that log-opts configuration options in the daemon.json configuration file must be provided as strings. Boolean and numeric values (such as the value for max-file in the example above) must therefore be enclosed in quotes (").

For instance, to define the default address pools, on can set the `daemon_config` variable as follows:

```yml
daemon_config:
  log-driver: "json-file"
  log-opts:
    max-size: "25m"
    max-file: "2"
  default-address-pools:
    - scope: "local"
      base: "172.16.0.0/12"
      size: 16
    - scope: "local"
      base: "192.168.0.0/16"
      size: 20
    - scope: "global"
      base: "10.0.0.0/8"
      size: 24
```
